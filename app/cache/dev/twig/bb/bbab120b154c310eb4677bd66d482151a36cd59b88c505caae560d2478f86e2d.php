<?php

/* TwigBundle:Exception:error.txt.twig */
class __TwigTemplate_8a7c4e5412c77475f25b1808f41eb9f83b17f9b2282998dd76de6beb114a20fe extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_c3a5d278e8cca0bd5face5796ab35361e0d163901c2b1c3e84ef9a5ac3d49066 = $this->env->getExtension("native_profiler");
        $__internal_c3a5d278e8cca0bd5face5796ab35361e0d163901c2b1c3e84ef9a5ac3d49066->enter($__internal_c3a5d278e8cca0bd5face5796ab35361e0d163901c2b1c3e84ef9a5ac3d49066_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.txt.twig"));

        // line 1
        echo "Oops! An Error Occurred
=======================

The server returned a \"";
        // line 4
        echo (isset($context["status_code"]) ? $context["status_code"] : $this->getContext($context, "status_code"));
        echo " ";
        echo (isset($context["status_text"]) ? $context["status_text"] : $this->getContext($context, "status_text"));
        echo "\".

Something is broken. Please let us know what you were doing when this error occurred.
We will fix it as soon as possible. Sorry for any inconvenience caused.
";
        
        $__internal_c3a5d278e8cca0bd5face5796ab35361e0d163901c2b1c3e84ef9a5ac3d49066->leave($__internal_c3a5d278e8cca0bd5face5796ab35361e0d163901c2b1c3e84ef9a5ac3d49066_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:error.txt.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  27 => 4,  22 => 1,);
    }
}
/* Oops! An Error Occurred*/
/* =======================*/
/* */
/* The server returned a "{{ status_code }} {{ status_text }}".*/
/* */
/* Something is broken. Please let us know what you were doing when this error occurred.*/
/* We will fix it as soon as possible. Sorry for any inconvenience caused.*/
/* */
