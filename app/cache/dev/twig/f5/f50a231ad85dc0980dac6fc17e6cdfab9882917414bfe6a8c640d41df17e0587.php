<?php

/* TwigBundle:Exception:error.css.twig */
class __TwigTemplate_184c585f4cdad959afa5c09e00ff966bcba4989aa08349ca4ee4c55932a184d6 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_811e7e03d90b7a93325e4a3e7b0bc1cec22fde675d00e18d10ea7b4801eb7cf0 = $this->env->getExtension("native_profiler");
        $__internal_811e7e03d90b7a93325e4a3e7b0bc1cec22fde675d00e18d10ea7b4801eb7cf0->enter($__internal_811e7e03d90b7a93325e4a3e7b0bc1cec22fde675d00e18d10ea7b4801eb7cf0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.css.twig"));

        // line 1
        echo "/*
";
        // line 2
        echo twig_escape_filter($this->env, (isset($context["status_code"]) ? $context["status_code"] : $this->getContext($context, "status_code")), "css", null, true);
        echo " ";
        echo twig_escape_filter($this->env, (isset($context["status_text"]) ? $context["status_text"] : $this->getContext($context, "status_text")), "css", null, true);
        echo "

*/
";
        
        $__internal_811e7e03d90b7a93325e4a3e7b0bc1cec22fde675d00e18d10ea7b4801eb7cf0->leave($__internal_811e7e03d90b7a93325e4a3e7b0bc1cec22fde675d00e18d10ea7b4801eb7cf0_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:error.css.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  25 => 2,  22 => 1,);
    }
}
/* /**/
/* {{ status_code }} {{ status_text }}*/
/* */
/* *//* */
/* */
