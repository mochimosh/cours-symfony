<?php

/* menu.html.twig */
class __TwigTemplate_4719463ed4be4257505f166a9e6e3adcb457724f8f744fbae195782a1d36f50f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_e24208bb25227cc89ca112e6aea85625389addbf7d243961b7e82a42a24f248f = $this->env->getExtension("native_profiler");
        $__internal_e24208bb25227cc89ca112e6aea85625389addbf7d243961b7e82a42a24f248f->enter($__internal_e24208bb25227cc89ca112e6aea85625389addbf7d243961b7e82a42a24f248f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "menu.html.twig"));

        // line 1
        echo "<nav>
    <ul>

        <li>
            <a href=\"";
        // line 5
        echo $this->env->getExtension('routing')->getPath("homepage");
        echo "\">Page d'accueil</a>
        </li>
        <li><a href=\"";
        // line 7
        echo $this->env->getExtension('routing')->getPath("lesson");
        echo "\">Voir les leçons</a></li>
        <li><a href=\"";
        // line 8
        echo $this->env->getExtension('routing')->getPath("grade");
        echo "\">Voir les notes</a></li>
    </ul>
</nav>";
        
        $__internal_e24208bb25227cc89ca112e6aea85625389addbf7d243961b7e82a42a24f248f->leave($__internal_e24208bb25227cc89ca112e6aea85625389addbf7d243961b7e82a42a24f248f_prof);

    }

    public function getTemplateName()
    {
        return "menu.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  37 => 8,  33 => 7,  28 => 5,  22 => 1,);
    }
}
/* <nav>*/
/*     <ul>*/
/* */
/*         <li>*/
/*             <a href="{{ path('homepage') }}">Page d'accueil</a>*/
/*         </li>*/
/*         <li><a href="{{ path('lesson') }}">Voir les leçons</a></li>*/
/*         <li><a href="{{ path('grade') }}">Voir les notes</a></li>*/
/*     </ul>*/
/* </nav>*/
