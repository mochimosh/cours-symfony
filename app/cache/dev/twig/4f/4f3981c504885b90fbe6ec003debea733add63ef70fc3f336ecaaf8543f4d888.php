<?php

/* TwigBundle:Exception:error.json.twig */
class __TwigTemplate_be3e92ac9cf199353fd3be41ece44d19dcccf9b9c0f890e4c5b16f09c91b270f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_fcc9fca60786709acb48110def781e1c6607d6ef0913715efa3c17e0840a47b9 = $this->env->getExtension("native_profiler");
        $__internal_fcc9fca60786709acb48110def781e1c6607d6ef0913715efa3c17e0840a47b9->enter($__internal_fcc9fca60786709acb48110def781e1c6607d6ef0913715efa3c17e0840a47b9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.json.twig"));

        // line 1
        echo twig_jsonencode_filter(array("error" => array("code" => (isset($context["status_code"]) ? $context["status_code"] : $this->getContext($context, "status_code")), "message" => (isset($context["status_text"]) ? $context["status_text"] : $this->getContext($context, "status_text")))));
        echo "
";
        
        $__internal_fcc9fca60786709acb48110def781e1c6607d6ef0913715efa3c17e0840a47b9->leave($__internal_fcc9fca60786709acb48110def781e1c6607d6ef0913715efa3c17e0840a47b9_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:error.json.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* {{ { 'error': { 'code': status_code, 'message': status_text } }|json_encode|raw }}*/
/* */
