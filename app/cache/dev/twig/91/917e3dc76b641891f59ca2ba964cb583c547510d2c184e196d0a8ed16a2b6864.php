<?php

/* TwigBundle:Exception:error.xml.twig */
class __TwigTemplate_41166f0741acbf385b621d0fbec523e4ca43a0ec86218de03e792a0fbcc3c397 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_c06b12d81958123670f6173f9ef208674e506e7aac6eff2962f9f6180109d033 = $this->env->getExtension("native_profiler");
        $__internal_c06b12d81958123670f6173f9ef208674e506e7aac6eff2962f9f6180109d033->enter($__internal_c06b12d81958123670f6173f9ef208674e506e7aac6eff2962f9f6180109d033_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.xml.twig"));

        // line 1
        echo "<?xml version=\"1.0\" encoding=\"";
        echo twig_escape_filter($this->env, $this->env->getCharset(), "html", null, true);
        echo "\" ?>

<error code=\"";
        // line 3
        echo twig_escape_filter($this->env, (isset($context["status_code"]) ? $context["status_code"] : $this->getContext($context, "status_code")), "html", null, true);
        echo "\" message=\"";
        echo twig_escape_filter($this->env, (isset($context["status_text"]) ? $context["status_text"] : $this->getContext($context, "status_text")), "html", null, true);
        echo "\" />
";
        
        $__internal_c06b12d81958123670f6173f9ef208674e506e7aac6eff2962f9f6180109d033->leave($__internal_c06b12d81958123670f6173f9ef208674e506e7aac6eff2962f9f6180109d033_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:error.xml.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  28 => 3,  22 => 1,);
    }
}
/* <?xml version="1.0" encoding="{{ _charset }}" ?>*/
/* */
/* <error code="{{ status_code }}" message="{{ status_text }}" />*/
/* */
