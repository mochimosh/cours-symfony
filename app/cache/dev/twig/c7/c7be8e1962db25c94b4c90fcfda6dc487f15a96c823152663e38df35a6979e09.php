<?php

/* base.html.twig */
class __TwigTemplate_740da53f77698065442e138a6867bb30c0bcde00d467bef2a32bd1e3a7c74acf extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_aad793d8ccc412f832366e15b8b7f88a28f6ff34851e93ad9bf11defc7901eaa = $this->env->getExtension("native_profiler");
        $__internal_aad793d8ccc412f832366e15b8b7f88a28f6ff34851e93ad9bf11defc7901eaa->enter($__internal_aad793d8ccc412f832366e15b8b7f88a28f6ff34851e93ad9bf11defc7901eaa_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "base.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <title>";
        // line 5
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        ";
        // line 6
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 11
        echo "        <link rel=\"icon\" type=\"image/x-icon\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("favicon.ico"), "html", null, true);
        echo "\" />
    </head>
    <body>
        ";
        // line 14
        $this->loadTemplate("menu.html.twig", "base.html.twig", 14)->display($context);
        // line 15
        echo "        ";
        $this->displayBlock('body', $context, $blocks);
        // line 16
        echo "        ";
        $this->displayBlock('javascripts', $context, $blocks);
        // line 21
        echo "    </body>
</html>
";
        
        $__internal_aad793d8ccc412f832366e15b8b7f88a28f6ff34851e93ad9bf11defc7901eaa->leave($__internal_aad793d8ccc412f832366e15b8b7f88a28f6ff34851e93ad9bf11defc7901eaa_prof);

    }

    // line 5
    public function block_title($context, array $blocks = array())
    {
        $__internal_b5a31e68b9a2d06c00d20a6e1423f0f6e84131222901c391d7b9f7c61ea58d6a = $this->env->getExtension("native_profiler");
        $__internal_b5a31e68b9a2d06c00d20a6e1423f0f6e84131222901c391d7b9f7c61ea58d6a->enter($__internal_b5a31e68b9a2d06c00d20a6e1423f0f6e84131222901c391d7b9f7c61ea58d6a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Welcome!";
        
        $__internal_b5a31e68b9a2d06c00d20a6e1423f0f6e84131222901c391d7b9f7c61ea58d6a->leave($__internal_b5a31e68b9a2d06c00d20a6e1423f0f6e84131222901c391d7b9f7c61ea58d6a_prof);

    }

    // line 6
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_0e00379665987ccf33a899858e7a030f88e5cba4408a08f97267ca2180e515ec = $this->env->getExtension("native_profiler");
        $__internal_0e00379665987ccf33a899858e7a030f88e5cba4408a08f97267ca2180e515ec->enter($__internal_0e00379665987ccf33a899858e7a030f88e5cba4408a08f97267ca2180e515ec_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 7
        echo "            ";
        if (isset($context['assetic']['debug']) && $context['assetic']['debug']) {
            // asset "dc4742d_0"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_dc4742d_0") : $this->env->getExtension('asset')->getAssetUrl("_controller/css/dc4742d_part_1.css");
            // line 8
            echo "                <link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\"/>
            ";
        } else {
            // asset "dc4742d"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_dc4742d") : $this->env->getExtension('asset')->getAssetUrl("_controller/css/dc4742d.css");
            echo "                <link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\"/>
            ";
        }
        unset($context["asset_url"]);
        // line 10
        echo "        ";
        
        $__internal_0e00379665987ccf33a899858e7a030f88e5cba4408a08f97267ca2180e515ec->leave($__internal_0e00379665987ccf33a899858e7a030f88e5cba4408a08f97267ca2180e515ec_prof);

    }

    // line 15
    public function block_body($context, array $blocks = array())
    {
        $__internal_1cd71967fce2719a7b802bd77a2f061d7eaf9871c8a451aeb50fbf55834192b5 = $this->env->getExtension("native_profiler");
        $__internal_1cd71967fce2719a7b802bd77a2f061d7eaf9871c8a451aeb50fbf55834192b5->enter($__internal_1cd71967fce2719a7b802bd77a2f061d7eaf9871c8a451aeb50fbf55834192b5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        
        $__internal_1cd71967fce2719a7b802bd77a2f061d7eaf9871c8a451aeb50fbf55834192b5->leave($__internal_1cd71967fce2719a7b802bd77a2f061d7eaf9871c8a451aeb50fbf55834192b5_prof);

    }

    // line 16
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_dd75a4c669d5e3573eca9ee701f3f06d95827e8fb0b6512f9c16c0eacdc11c36 = $this->env->getExtension("native_profiler");
        $__internal_dd75a4c669d5e3573eca9ee701f3f06d95827e8fb0b6512f9c16c0eacdc11c36->enter($__internal_dd75a4c669d5e3573eca9ee701f3f06d95827e8fb0b6512f9c16c0eacdc11c36_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        // line 17
        echo "            ";
        if (isset($context['assetic']['debug']) && $context['assetic']['debug']) {
            // asset "4d1d496_0"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_4d1d496_0") : $this->env->getExtension('asset')->getAssetUrl("_controller/js/4d1d496_part_1.js");
            // line 18
            echo "                <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\"></script>
            ";
        } else {
            // asset "4d1d496"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_4d1d496") : $this->env->getExtension('asset')->getAssetUrl("_controller/js/4d1d496.js");
            echo "                <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\"></script>
            ";
        }
        unset($context["asset_url"]);
        // line 20
        echo "        ";
        
        $__internal_dd75a4c669d5e3573eca9ee701f3f06d95827e8fb0b6512f9c16c0eacdc11c36->leave($__internal_dd75a4c669d5e3573eca9ee701f3f06d95827e8fb0b6512f9c16c0eacdc11c36_prof);

    }

    public function getTemplateName()
    {
        return "base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  142 => 20,  128 => 18,  123 => 17,  117 => 16,  106 => 15,  99 => 10,  85 => 8,  80 => 7,  74 => 6,  62 => 5,  53 => 21,  50 => 16,  47 => 15,  45 => 14,  38 => 11,  36 => 6,  32 => 5,  26 => 1,);
    }
}
/* <!DOCTYPE html>*/
/* <html>*/
/*     <head>*/
/*         <meta charset="UTF-8" />*/
/*         <title>{% block title %}Welcome!{% endblock %}</title>*/
/*         {% block stylesheets%}*/
/*             {% stylesheets '@adminlte_css' %}*/
/*                 <link rel="stylesheet" href="{{ asset_url }}"/>*/
/*             {% endstylesheets %}*/
/*         {% endblock %}*/
/*         <link rel="icon" type="image/x-icon" href="{{ asset('favicon.ico') }}" />*/
/*     </head>*/
/*     <body>*/
/*         {% include('menu.html.twig')  %}*/
/*         {% block body %}{% endblock %}*/
/*         {% block javascripts %}*/
/*             {% javascripts '@adminlte_js' %}*/
/*                 <script src="{{ asset_url }}"></script>*/
/*             {% endjavascripts %}*/
/*         {% endblock %}*/
/*     </body>*/
/* </html>*/
/* */
