<?php

/* TwigBundle:Exception:exception.rdf.twig */
class __TwigTemplate_b24efe75d8b570cff9ecfc39567165f01fdd82be5a6e8dc2d4fa73f9abe1a1d1 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_6f1cda23f31e61d8410320b801244f3c0506518b73810b3209b0dc3a439c5f4d = $this->env->getExtension("native_profiler");
        $__internal_6f1cda23f31e61d8410320b801244f3c0506518b73810b3209b0dc3a439c5f4d->enter($__internal_6f1cda23f31e61d8410320b801244f3c0506518b73810b3209b0dc3a439c5f4d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.rdf.twig"));

        // line 1
        $this->loadTemplate("TwigBundle:Exception:exception.xml.twig", "TwigBundle:Exception:exception.rdf.twig", 1)->display(array_merge($context, array("exception" => (isset($context["exception"]) ? $context["exception"] : $this->getContext($context, "exception")))));
        
        $__internal_6f1cda23f31e61d8410320b801244f3c0506518b73810b3209b0dc3a439c5f4d->leave($__internal_6f1cda23f31e61d8410320b801244f3c0506518b73810b3209b0dc3a439c5f4d_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:exception.rdf.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* {% include 'TwigBundle:Exception:exception.xml.twig' with { 'exception': exception } %}*/
/* */
