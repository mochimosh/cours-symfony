<?php

/* TwigBundle:Exception:exception.atom.twig */
class __TwigTemplate_9d3c38259284985fd1a34696f5f739d97d1831d68a96f35a60c3050d2c884d1e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_31dc7cb72bbb773b1446b0ee06135f7e40a4d31854b7071bb8a2453ba0bcd03d = $this->env->getExtension("native_profiler");
        $__internal_31dc7cb72bbb773b1446b0ee06135f7e40a4d31854b7071bb8a2453ba0bcd03d->enter($__internal_31dc7cb72bbb773b1446b0ee06135f7e40a4d31854b7071bb8a2453ba0bcd03d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.atom.twig"));

        // line 1
        $this->loadTemplate("TwigBundle:Exception:exception.xml.twig", "TwigBundle:Exception:exception.atom.twig", 1)->display(array_merge($context, array("exception" => (isset($context["exception"]) ? $context["exception"] : $this->getContext($context, "exception")))));
        
        $__internal_31dc7cb72bbb773b1446b0ee06135f7e40a4d31854b7071bb8a2453ba0bcd03d->leave($__internal_31dc7cb72bbb773b1446b0ee06135f7e40a4d31854b7071bb8a2453ba0bcd03d_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:exception.atom.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* {% include 'TwigBundle:Exception:exception.xml.twig' with { 'exception': exception } %}*/
/* */
