<?php

/* :admin:createUser.html.twig */
class __TwigTemplate_ed8974b2fc49120d832075be7166e2a03af8c5518950113d54bb957097203b00 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", ":admin:createUser.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_e24862c34c3152901360d39d591fa7bb119e8a120bcfaf343a81c8d75bdbeca2 = $this->env->getExtension("native_profiler");
        $__internal_e24862c34c3152901360d39d591fa7bb119e8a120bcfaf343a81c8d75bdbeca2->enter($__internal_e24862c34c3152901360d39d591fa7bb119e8a120bcfaf343a81c8d75bdbeca2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", ":admin:createUser.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_e24862c34c3152901360d39d591fa7bb119e8a120bcfaf343a81c8d75bdbeca2->leave($__internal_e24862c34c3152901360d39d591fa7bb119e8a120bcfaf343a81c8d75bdbeca2_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_f185871c5548e4f309982ab970f8c0f7164659586334e0873672c24c0d67ebe6 = $this->env->getExtension("native_profiler");
        $__internal_f185871c5548e4f309982ab970f8c0f7164659586334e0873672c24c0d67ebe6->enter($__internal_f185871c5548e4f309982ab970f8c0f7164659586334e0873672c24c0d67ebe6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "    ";
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_start');
        echo "
    ";
        // line 5
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "username", array()), 'row');
        echo "
    ";
        // line 6
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "email", array()), 'row');
        echo "
    ";
        // line 7
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "plainPassword", array()), "first", array()), 'row');
        echo "
    ";
        // line 8
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "plainPassword", array()), "second", array()), 'row');
        echo "

    <button type=\"submit\">Register!</button>
    ";
        // line 11
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_end');
        echo "
";
        
        $__internal_f185871c5548e4f309982ab970f8c0f7164659586334e0873672c24c0d67ebe6->leave($__internal_f185871c5548e4f309982ab970f8c0f7164659586334e0873672c24c0d67ebe6_prof);

    }

    public function getTemplateName()
    {
        return ":admin:createUser.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  63 => 11,  57 => 8,  53 => 7,  49 => 6,  45 => 5,  40 => 4,  34 => 3,  11 => 1,);
    }
}
/* {% extends 'base.html.twig' %}*/
/* */
/* {% block body %}*/
/*     {{ form_start(form) }}*/
/*     {{ form_row(form.username) }}*/
/*     {{ form_row(form.email) }}*/
/*     {{ form_row(form.plainPassword.first) }}*/
/*     {{ form_row(form.plainPassword.second) }}*/
/* */
/*     <button type="submit">Register!</button>*/
/*     {{ form_end(form) }}*/
/* {% endblock %}*/
