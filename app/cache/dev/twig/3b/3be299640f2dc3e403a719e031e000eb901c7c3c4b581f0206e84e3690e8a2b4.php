<?php

/* WebProfilerBundle:Profiler:ajax_layout.html.twig */
class __TwigTemplate_385aad5ca0c8c393aa289aed8bfc7b165ae9508d07d6913640c659d9914d6e2d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_381893d55a0a39fabc35d2787e753bd6882ac1f147f8fbf8dd47b9ca7bde745a = $this->env->getExtension("native_profiler");
        $__internal_381893d55a0a39fabc35d2787e753bd6882ac1f147f8fbf8dd47b9ca7bde745a->enter($__internal_381893d55a0a39fabc35d2787e753bd6882ac1f147f8fbf8dd47b9ca7bde745a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:ajax_layout.html.twig"));

        // line 1
        $this->displayBlock('panel', $context, $blocks);
        
        $__internal_381893d55a0a39fabc35d2787e753bd6882ac1f147f8fbf8dd47b9ca7bde745a->leave($__internal_381893d55a0a39fabc35d2787e753bd6882ac1f147f8fbf8dd47b9ca7bde745a_prof);

    }

    public function block_panel($context, array $blocks = array())
    {
        $__internal_d05ab8f2f1a9b9a366db860adef9369340d293c54c40da96ed520767cdc9f1c9 = $this->env->getExtension("native_profiler");
        $__internal_d05ab8f2f1a9b9a366db860adef9369340d293c54c40da96ed520767cdc9f1c9->enter($__internal_d05ab8f2f1a9b9a366db860adef9369340d293c54c40da96ed520767cdc9f1c9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        echo "";
        
        $__internal_d05ab8f2f1a9b9a366db860adef9369340d293c54c40da96ed520767cdc9f1c9->leave($__internal_d05ab8f2f1a9b9a366db860adef9369340d293c54c40da96ed520767cdc9f1c9_prof);

    }

    public function getTemplateName()
    {
        return "WebProfilerBundle:Profiler:ajax_layout.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  23 => 1,);
    }
}
/* {% block panel '' %}*/
/* */
