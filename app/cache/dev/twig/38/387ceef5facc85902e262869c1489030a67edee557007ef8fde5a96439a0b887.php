<?php

/* SensioDistributionBundle::Configurator/layout.html.twig */
class __TwigTemplate_bbba6571b3836cb65855072a3ce80f4bad300fc33dc1f25f9128b7a2c2183dba extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("TwigBundle::layout.html.twig", "SensioDistributionBundle::Configurator/layout.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "TwigBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_dab32fb2d537120cb362a50bc86facde96bf3e45cb908730ad3a48d648115b82 = $this->env->getExtension("native_profiler");
        $__internal_dab32fb2d537120cb362a50bc86facde96bf3e45cb908730ad3a48d648115b82->enter($__internal_dab32fb2d537120cb362a50bc86facde96bf3e45cb908730ad3a48d648115b82_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SensioDistributionBundle::Configurator/layout.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_dab32fb2d537120cb362a50bc86facde96bf3e45cb908730ad3a48d648115b82->leave($__internal_dab32fb2d537120cb362a50bc86facde96bf3e45cb908730ad3a48d648115b82_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_2591e6aa02baf844bfc8a299f4d997f1aa0c4a49adc391bb7be08e1d16371c6d = $this->env->getExtension("native_profiler");
        $__internal_2591e6aa02baf844bfc8a299f4d997f1aa0c4a49adc391bb7be08e1d16371c6d->enter($__internal_2591e6aa02baf844bfc8a299f4d997f1aa0c4a49adc391bb7be08e1d16371c6d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    <link rel=\"stylesheet\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/sensiodistribution/webconfigurator/css/configurator.css"), "html", null, true);
        echo "\" />
";
        
        $__internal_2591e6aa02baf844bfc8a299f4d997f1aa0c4a49adc391bb7be08e1d16371c6d->leave($__internal_2591e6aa02baf844bfc8a299f4d997f1aa0c4a49adc391bb7be08e1d16371c6d_prof);

    }

    // line 7
    public function block_title($context, array $blocks = array())
    {
        $__internal_67db5d27c2baa943da5149d1825576fee2755032484eeb4d2a3653f502c81821 = $this->env->getExtension("native_profiler");
        $__internal_67db5d27c2baa943da5149d1825576fee2755032484eeb4d2a3653f502c81821->enter($__internal_67db5d27c2baa943da5149d1825576fee2755032484eeb4d2a3653f502c81821_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Web Configurator Bundle";
        
        $__internal_67db5d27c2baa943da5149d1825576fee2755032484eeb4d2a3653f502c81821->leave($__internal_67db5d27c2baa943da5149d1825576fee2755032484eeb4d2a3653f502c81821_prof);

    }

    // line 9
    public function block_body($context, array $blocks = array())
    {
        $__internal_bf04d8e04e665a04c34e90ecf314dad38421bc8cccdaa1e23522b3eb75aa12d9 = $this->env->getExtension("native_profiler");
        $__internal_bf04d8e04e665a04c34e90ecf314dad38421bc8cccdaa1e23522b3eb75aa12d9->enter($__internal_bf04d8e04e665a04c34e90ecf314dad38421bc8cccdaa1e23522b3eb75aa12d9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 10
        echo "    <div class=\"block\">
        ";
        // line 11
        $this->displayBlock('content', $context, $blocks);
        // line 12
        echo "    </div>
    <div class=\"version\">Symfony Standard Edition v.";
        // line 13
        echo twig_escape_filter($this->env, (isset($context["version"]) ? $context["version"] : $this->getContext($context, "version")), "html", null, true);
        echo "</div>
";
        
        $__internal_bf04d8e04e665a04c34e90ecf314dad38421bc8cccdaa1e23522b3eb75aa12d9->leave($__internal_bf04d8e04e665a04c34e90ecf314dad38421bc8cccdaa1e23522b3eb75aa12d9_prof);

    }

    // line 11
    public function block_content($context, array $blocks = array())
    {
        $__internal_1b9e6e4d779c1520f5916ae22233d5245cefd2de608e5ce1f64f59a30657e2c5 = $this->env->getExtension("native_profiler");
        $__internal_1b9e6e4d779c1520f5916ae22233d5245cefd2de608e5ce1f64f59a30657e2c5->enter($__internal_1b9e6e4d779c1520f5916ae22233d5245cefd2de608e5ce1f64f59a30657e2c5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        
        $__internal_1b9e6e4d779c1520f5916ae22233d5245cefd2de608e5ce1f64f59a30657e2c5->leave($__internal_1b9e6e4d779c1520f5916ae22233d5245cefd2de608e5ce1f64f59a30657e2c5_prof);

    }

    public function getTemplateName()
    {
        return "SensioDistributionBundle::Configurator/layout.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  88 => 11,  79 => 13,  76 => 12,  74 => 11,  71 => 10,  65 => 9,  53 => 7,  43 => 4,  37 => 3,  11 => 1,);
    }
}
/* {% extends "TwigBundle::layout.html.twig" %}*/
/* */
/* {% block head %}*/
/*     <link rel="stylesheet" href="{{ asset('bundles/sensiodistribution/webconfigurator/css/configurator.css') }}" />*/
/* {% endblock %}*/
/* */
/* {% block title 'Web Configurator Bundle' %}*/
/* */
/* {% block body %}*/
/*     <div class="block">*/
/*         {% block content %}{% endblock %}*/
/*     </div>*/
/*     <div class="version">Symfony Standard Edition v.{{ version }}</div>*/
/* {% endblock %}*/
/* */
