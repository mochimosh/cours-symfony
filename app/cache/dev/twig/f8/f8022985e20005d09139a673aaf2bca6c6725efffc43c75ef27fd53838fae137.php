<?php

/* TwigBundle:Exception:exception_full.html.twig */
class __TwigTemplate_83aea1360b663bffe38c8f1988bce0ca013fd82a2464fe4b3fe9bd076ecc3024 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("TwigBundle::layout.html.twig", "TwigBundle:Exception:exception_full.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "TwigBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_dec7c3bb78761052f79d933e0a66bb79dbdcffa4a7707b118fd4c4c3c37e9f86 = $this->env->getExtension("native_profiler");
        $__internal_dec7c3bb78761052f79d933e0a66bb79dbdcffa4a7707b118fd4c4c3c37e9f86->enter($__internal_dec7c3bb78761052f79d933e0a66bb79dbdcffa4a7707b118fd4c4c3c37e9f86_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception_full.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_dec7c3bb78761052f79d933e0a66bb79dbdcffa4a7707b118fd4c4c3c37e9f86->leave($__internal_dec7c3bb78761052f79d933e0a66bb79dbdcffa4a7707b118fd4c4c3c37e9f86_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_541cd039babd7e2a0c7e0961d3cf5777fe3b94fc981a9e7d024f677b37b151c2 = $this->env->getExtension("native_profiler");
        $__internal_541cd039babd7e2a0c7e0961d3cf5777fe3b94fc981a9e7d024f677b37b151c2->enter($__internal_541cd039babd7e2a0c7e0961d3cf5777fe3b94fc981a9e7d024f677b37b151c2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    <link href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('request')->generateAbsoluteUrl($this->env->getExtension('asset')->getAssetUrl("bundles/framework/css/exception.css")), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\" media=\"all\" />
";
        
        $__internal_541cd039babd7e2a0c7e0961d3cf5777fe3b94fc981a9e7d024f677b37b151c2->leave($__internal_541cd039babd7e2a0c7e0961d3cf5777fe3b94fc981a9e7d024f677b37b151c2_prof);

    }

    // line 7
    public function block_title($context, array $blocks = array())
    {
        $__internal_8b18e805f8f42d83f311226658d69fd098abb5c62c1dfca21aa94b1c334b32be = $this->env->getExtension("native_profiler");
        $__internal_8b18e805f8f42d83f311226658d69fd098abb5c62c1dfca21aa94b1c334b32be->enter($__internal_8b18e805f8f42d83f311226658d69fd098abb5c62c1dfca21aa94b1c334b32be_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        // line 8
        echo "    ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["exception"]) ? $context["exception"] : $this->getContext($context, "exception")), "message", array()), "html", null, true);
        echo " (";
        echo twig_escape_filter($this->env, (isset($context["status_code"]) ? $context["status_code"] : $this->getContext($context, "status_code")), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, (isset($context["status_text"]) ? $context["status_text"] : $this->getContext($context, "status_text")), "html", null, true);
        echo ")
";
        
        $__internal_8b18e805f8f42d83f311226658d69fd098abb5c62c1dfca21aa94b1c334b32be->leave($__internal_8b18e805f8f42d83f311226658d69fd098abb5c62c1dfca21aa94b1c334b32be_prof);

    }

    // line 11
    public function block_body($context, array $blocks = array())
    {
        $__internal_14758de5863836fa0fd58d6bd207922bbf637104fbea7b90696dd17220ac663a = $this->env->getExtension("native_profiler");
        $__internal_14758de5863836fa0fd58d6bd207922bbf637104fbea7b90696dd17220ac663a->enter($__internal_14758de5863836fa0fd58d6bd207922bbf637104fbea7b90696dd17220ac663a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 12
        echo "    ";
        $this->loadTemplate("TwigBundle:Exception:exception.html.twig", "TwigBundle:Exception:exception_full.html.twig", 12)->display($context);
        
        $__internal_14758de5863836fa0fd58d6bd207922bbf637104fbea7b90696dd17220ac663a->leave($__internal_14758de5863836fa0fd58d6bd207922bbf637104fbea7b90696dd17220ac663a_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:exception_full.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  78 => 12,  72 => 11,  58 => 8,  52 => 7,  42 => 4,  36 => 3,  11 => 1,);
    }
}
/* {% extends 'TwigBundle::layout.html.twig' %}*/
/* */
/* {% block head %}*/
/*     <link href="{{ absolute_url(asset('bundles/framework/css/exception.css')) }}" rel="stylesheet" type="text/css" media="all" />*/
/* {% endblock %}*/
/* */
/* {% block title %}*/
/*     {{ exception.message }} ({{ status_code }} {{ status_text }})*/
/* {% endblock %}*/
/* */
/* {% block body %}*/
/*     {% include 'TwigBundle:Exception:exception.html.twig' %}*/
/* {% endblock %}*/
/* */
