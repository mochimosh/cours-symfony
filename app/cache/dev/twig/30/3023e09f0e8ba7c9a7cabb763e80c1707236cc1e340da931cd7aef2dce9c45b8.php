<?php

/* :admin:index.html.twig */
class __TwigTemplate_6c25e1271f67293435ca0c9087fc37afb3ab455bf7c8b80925544b204991a948 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", ":admin:index.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_c4b650be8334a45268a9049e2f15fff5e9559a75a59b6cfc517b5ec7c552a063 = $this->env->getExtension("native_profiler");
        $__internal_c4b650be8334a45268a9049e2f15fff5e9559a75a59b6cfc517b5ec7c552a063->enter($__internal_c4b650be8334a45268a9049e2f15fff5e9559a75a59b6cfc517b5ec7c552a063_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", ":admin:index.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_c4b650be8334a45268a9049e2f15fff5e9559a75a59b6cfc517b5ec7c552a063->leave($__internal_c4b650be8334a45268a9049e2f15fff5e9559a75a59b6cfc517b5ec7c552a063_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_be5de70da481bb3dd1812382be3dc8f1fb6a8a5880de053c2dc9a1f67ccb3d24 = $this->env->getExtension("native_profiler");
        $__internal_be5de70da481bb3dd1812382be3dc8f1fb6a8a5880de053c2dc9a1f67ccb3d24->enter($__internal_be5de70da481bb3dd1812382be3dc8f1fb6a8a5880de053c2dc9a1f67ccb3d24_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "    <h1>Bienvenue ";
        echo twig_escape_filter($this->env, (isset($context["user"]) ? $context["user"] : $this->getContext($context, "user")), "html", null, true);
        echo "</h1>
";
        
        $__internal_be5de70da481bb3dd1812382be3dc8f1fb6a8a5880de053c2dc9a1f67ccb3d24->leave($__internal_be5de70da481bb3dd1812382be3dc8f1fb6a8a5880de053c2dc9a1f67ccb3d24_prof);

    }

    public function getTemplateName()
    {
        return ":admin:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  40 => 4,  34 => 3,  11 => 1,);
    }
}
/* {% extends 'base.html.twig' %}*/
/* */
/* {% block body %}*/
/*     <h1>Bienvenue {{ user }}</h1>*/
/* {% endblock %}*/
/* */
