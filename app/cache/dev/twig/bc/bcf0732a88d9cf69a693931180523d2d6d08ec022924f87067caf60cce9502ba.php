<?php

/* :lesson:lessons.html.twig */
class __TwigTemplate_bc8d9957595a9605c84b9d1f7c7cdc1c23b7553d4cb480208895f94cc493f78c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", ":lesson:lessons.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_17ef0b89387ca8f3029d1b1af18cae57cc3ecdc4d2bd5bb98688348359679eae = $this->env->getExtension("native_profiler");
        $__internal_17ef0b89387ca8f3029d1b1af18cae57cc3ecdc4d2bd5bb98688348359679eae->enter($__internal_17ef0b89387ca8f3029d1b1af18cae57cc3ecdc4d2bd5bb98688348359679eae_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", ":lesson:lessons.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_17ef0b89387ca8f3029d1b1af18cae57cc3ecdc4d2bd5bb98688348359679eae->leave($__internal_17ef0b89387ca8f3029d1b1af18cae57cc3ecdc4d2bd5bb98688348359679eae_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_a82e3d73b7d7fb990301b29b1cd00f50d435ae88cd17edb0d570ba7e5ee37e8c = $this->env->getExtension("native_profiler");
        $__internal_a82e3d73b7d7fb990301b29b1cd00f50d435ae88cd17edb0d570ba7e5ee37e8c->enter($__internal_a82e3d73b7d7fb990301b29b1cd00f50d435ae88cd17edb0d570ba7e5ee37e8c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "    <section>
        ";
        // line 5
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["lessons"]) ? $context["lessons"] : $this->getContext($context, "lessons")));
        foreach ($context['_seq'] as $context["_key"] => $context["lesson"]) {
            // line 6
            echo "            <article>
                <h1>";
            // line 7
            echo twig_escape_filter($this->env, $this->getAttribute($context["lesson"], "title", array()), "html", null, true);
            echo "</h1>
                <p>";
            // line 8
            echo twig_escape_filter($this->env, $this->getAttribute($context["lesson"], "summary", array()), "html", null, true);
            echo "</p>
                <p>";
            // line 9
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["lesson"], "writer", array()), "username", array()), "html", null, true);
            echo "</p>
            </article>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['lesson'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 12
        echo "    </section>
";
        
        $__internal_a82e3d73b7d7fb990301b29b1cd00f50d435ae88cd17edb0d570ba7e5ee37e8c->leave($__internal_a82e3d73b7d7fb990301b29b1cd00f50d435ae88cd17edb0d570ba7e5ee37e8c_prof);

    }

    public function getTemplateName()
    {
        return ":lesson:lessons.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  67 => 12,  58 => 9,  54 => 8,  50 => 7,  47 => 6,  43 => 5,  40 => 4,  34 => 3,  11 => 1,);
    }
}
/* {% extends 'base.html.twig' %}*/
/* */
/* {% block body %}*/
/*     <section>*/
/*         {% for lesson in lessons %}*/
/*             <article>*/
/*                 <h1>{{ lesson.title }}</h1>*/
/*                 <p>{{ lesson.summary }}</p>*/
/*                 <p>{{ lesson.writer.username }}</p>*/
/*             </article>*/
/*         {% endfor %}*/
/*     </section>*/
/* {% endblock %}*/
/* */
