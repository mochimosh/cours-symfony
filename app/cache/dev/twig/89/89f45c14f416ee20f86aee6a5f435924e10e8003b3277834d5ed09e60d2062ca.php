<?php

/* :grade:create.html.twig */
class __TwigTemplate_9ef3d177c6a41e6da72bee986fefe18a1e18e75866d6289d17b2d3bbf1495afc extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", ":grade:create.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_b5ac0dfc2792f595584ea4a393c11487b67cf5bb811ab115bc65d88276f6598a = $this->env->getExtension("native_profiler");
        $__internal_b5ac0dfc2792f595584ea4a393c11487b67cf5bb811ab115bc65d88276f6598a->enter($__internal_b5ac0dfc2792f595584ea4a393c11487b67cf5bb811ab115bc65d88276f6598a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", ":grade:create.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_b5ac0dfc2792f595584ea4a393c11487b67cf5bb811ab115bc65d88276f6598a->leave($__internal_b5ac0dfc2792f595584ea4a393c11487b67cf5bb811ab115bc65d88276f6598a_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_44d04e90c74a303ebd5628307e0ca1f592a977f6c913f9757a31a0bf06978c31 = $this->env->getExtension("native_profiler");
        $__internal_44d04e90c74a303ebd5628307e0ca1f592a977f6c913f9757a31a0bf06978c31->enter($__internal_44d04e90c74a303ebd5628307e0ca1f592a977f6c913f9757a31a0bf06978c31_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "    ";
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form');
        echo "
";
        
        $__internal_44d04e90c74a303ebd5628307e0ca1f592a977f6c913f9757a31a0bf06978c31->leave($__internal_44d04e90c74a303ebd5628307e0ca1f592a977f6c913f9757a31a0bf06978c31_prof);

    }

    public function getTemplateName()
    {
        return ":grade:create.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  40 => 4,  34 => 3,  11 => 1,);
    }
}
/* {% extends 'base.html.twig' %}*/
/* */
/* {% block body %}*/
/*     {{ form(form) }}*/
/* {% endblock %}*/
/* */
