<?php

/* TwigBundle:Exception:error.js.twig */
class __TwigTemplate_afed2982ae5207406c4da043a3d003f7c8d62c5cd2805997c22f1ee507ca1323 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_3863a1ecf45aa7cda30969d40a081148e05f4502fdc333bb65284dfb70685922 = $this->env->getExtension("native_profiler");
        $__internal_3863a1ecf45aa7cda30969d40a081148e05f4502fdc333bb65284dfb70685922->enter($__internal_3863a1ecf45aa7cda30969d40a081148e05f4502fdc333bb65284dfb70685922_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.js.twig"));

        // line 1
        echo "/*
";
        // line 2
        echo twig_escape_filter($this->env, (isset($context["status_code"]) ? $context["status_code"] : $this->getContext($context, "status_code")), "js", null, true);
        echo " ";
        echo twig_escape_filter($this->env, (isset($context["status_text"]) ? $context["status_text"] : $this->getContext($context, "status_text")), "js", null, true);
        echo "

*/
";
        
        $__internal_3863a1ecf45aa7cda30969d40a081148e05f4502fdc333bb65284dfb70685922->leave($__internal_3863a1ecf45aa7cda30969d40a081148e05f4502fdc333bb65284dfb70685922_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:error.js.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  25 => 2,  22 => 1,);
    }
}
/* /**/
/* {{ status_code }} {{ status_text }}*/
/* */
/* *//* */
/* */
