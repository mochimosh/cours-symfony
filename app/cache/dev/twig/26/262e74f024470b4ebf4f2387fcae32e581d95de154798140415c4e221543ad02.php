<?php

/* :registration:register.html.twig */
class __TwigTemplate_34439f3cf791acfaad2ed2b792db1df272ce24f67c0028993aee3b70e16453ab extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", ":registration:register.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_a2c40a45de541268619f0762e407fee8c5e93300a9392ca32238734bc5a84072 = $this->env->getExtension("native_profiler");
        $__internal_a2c40a45de541268619f0762e407fee8c5e93300a9392ca32238734bc5a84072->enter($__internal_a2c40a45de541268619f0762e407fee8c5e93300a9392ca32238734bc5a84072_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", ":registration:register.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_a2c40a45de541268619f0762e407fee8c5e93300a9392ca32238734bc5a84072->leave($__internal_a2c40a45de541268619f0762e407fee8c5e93300a9392ca32238734bc5a84072_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_4d33de4d7f2c41a6824f75b84af6ef5b890c54d28cada76aec4bd7f052826889 = $this->env->getExtension("native_profiler");
        $__internal_4d33de4d7f2c41a6824f75b84af6ef5b890c54d28cada76aec4bd7f052826889->enter($__internal_4d33de4d7f2c41a6824f75b84af6ef5b890c54d28cada76aec4bd7f052826889_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "    ";
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_start');
        echo "
        ";
        // line 5
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "username", array()), 'row');
        echo "
        ";
        // line 6
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "email", array()), 'row');
        echo "
        ";
        // line 7
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "plainPassword", array()), "first", array()), 'row');
        echo "
        ";
        // line 8
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "plainPassword", array()), "second", array()), 'row');
        echo "

        <button type=\"submit\">Register!</button>
    ";
        // line 11
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_end');
        echo "
";
        
        $__internal_4d33de4d7f2c41a6824f75b84af6ef5b890c54d28cada76aec4bd7f052826889->leave($__internal_4d33de4d7f2c41a6824f75b84af6ef5b890c54d28cada76aec4bd7f052826889_prof);

    }

    public function getTemplateName()
    {
        return ":registration:register.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  63 => 11,  57 => 8,  53 => 7,  49 => 6,  45 => 5,  40 => 4,  34 => 3,  11 => 1,);
    }
}
/* {% extends 'base.html.twig' %}*/
/* */
/* {% block body %}*/
/*     {{ form_start(form) }}*/
/*         {{ form_row(form.username) }}*/
/*         {{ form_row(form.email) }}*/
/*         {{ form_row(form.plainPassword.first) }}*/
/*         {{ form_row(form.plainPassword.second) }}*/
/* */
/*         <button type="submit">Register!</button>*/
/*     {{ form_end(form) }}*/
/* {% endblock %}*/
