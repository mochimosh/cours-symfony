<?php

/* :lesson:create.html.twig */
class __TwigTemplate_cd61cbe3324c4a72a41c7ffab046b812f25f702174f0259edf7bcd032553e723 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", ":lesson:create.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_a5f7885a998b3567a60832eb4a526faa5c15ad56387e811927a164e20a5208b9 = $this->env->getExtension("native_profiler");
        $__internal_a5f7885a998b3567a60832eb4a526faa5c15ad56387e811927a164e20a5208b9->enter($__internal_a5f7885a998b3567a60832eb4a526faa5c15ad56387e811927a164e20a5208b9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", ":lesson:create.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_a5f7885a998b3567a60832eb4a526faa5c15ad56387e811927a164e20a5208b9->leave($__internal_a5f7885a998b3567a60832eb4a526faa5c15ad56387e811927a164e20a5208b9_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_00b170de8952cd6534a0ad6dd443fde17670fd664ecff33abb02ffb30030cb37 = $this->env->getExtension("native_profiler");
        $__internal_00b170de8952cd6534a0ad6dd443fde17670fd664ecff33abb02ffb30030cb37->enter($__internal_00b170de8952cd6534a0ad6dd443fde17670fd664ecff33abb02ffb30030cb37_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "    ";
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form');
        echo "
";
        
        $__internal_00b170de8952cd6534a0ad6dd443fde17670fd664ecff33abb02ffb30030cb37->leave($__internal_00b170de8952cd6534a0ad6dd443fde17670fd664ecff33abb02ffb30030cb37_prof);

    }

    public function getTemplateName()
    {
        return ":lesson:create.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  40 => 4,  34 => 3,  11 => 1,);
    }
}
/* {% extends 'base.html.twig' %}*/
/* */
/* {% block body %}*/
/*     {{ form(form) }}*/
/* {% endblock %}*/
/* */
