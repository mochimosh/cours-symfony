<?php
/**
 * Created by PhpStorm.
 * User: guillaumeguerin
 * Date: 22/11/15
 * Time: 21:35
 */

namespace AppBundle\Controller;

use AppBundle\Form\UserType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class AdminController extends Controller
{

    /**
     * @Route("/admin", name="admin_home")
     */
    public function indexAction()
    {
        $user = $this->getUser();
        return $this->render('admin/index.html.twig', array(
            'name' => $user
        ));
    }

    /**
     * @Route("/admin/createUser", name="createUser")
     */
    public function createUserAction()
    {
        $user = new User();

        $form = $this->createForm(new UserType(), $user);

        $form->handleRequest($request);

        if ($request->isMethod('POST') && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();

            return $this->redirect($this->generateUrl('admin'));
        }

        return $this->render('admin/createUser.html.twig', [
            'form' => $form->createView()
        ]);
    }
}